import java.awt.image.BufferedImage;

import java.io.File;
import java.util.UUID;

import com.vividsolutions.jts.geom.Geometry;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.datasyslab.geospark.formatMapper.GeoJsonReader;
import org.datasyslab.geospark.spatialRDD.SpatialRDD;
import org.datasyslab.geosparksql.utils.Adapter;
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator;
import org.datasyslab.geosparkviz.core.ImageGenerator;
import org.datasyslab.geosparkviz.core.ImageSerializableWrapper;
import org.datasyslab.geosparkviz.core.Serde.GeoSparkVizKryoRegistrator;
import org.datasyslab.geosparkviz.sql.utils.GeoSparkVizRegistrator;
import org.datasyslab.geosparkviz.utils.ImageType;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;


public class GeoSparkVisualize {
    public static void main(String[] args) throws Exception {
        String inputFile = new File(args[0]).getAbsolutePath();
        String outputFile = new File(inputFile).getParent() + File.separator + UUID.randomUUID().toString();

        Logger.getLogger("org").setLevel(Level.WARN);
        Logger.getLogger("akka").setLevel(Level.WARN);

        SparkConf conf = new SparkConf().setAppName("adapterTestJava").setMaster("local[*]");
        conf.set("spark.serializer", org.apache.spark.serializer.KryoSerializer.class.getName());
        conf.set("spark.kryo.registrator", GeoSparkVizKryoRegistrator.class.getName());

        JavaSparkContext sc = new JavaSparkContext(conf);

        SparkSession sparkSession = new SparkSession(sc.sc());

        GeoSparkSQLRegistrator.registerAll(sparkSession);
        GeoSparkVizRegistrator.registerAll(sparkSession);

        SpatialRDD<Geometry> rdd = GeoJsonReader.readToGeometryRDD(sc, inputFile, true, true);

        Dataset<Row> df = Adapter.toDf(rdd, sparkSession);
        df.createOrReplaceTempView("pointtable");
        
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW pointtable AS\n" +
                "SELECT ST_GeomFromWKT(geometry) as shape\n" +
                "FROM pointtable");
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW boundtable AS\n" +
                "SELECT ST_Envelope_Aggr(shape) as bound FROM pointtable");
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW pixels AS\n" +
                "SELECT pixel, shape FROM pointtable\n" +
                "LATERAL VIEW ST_Pixelize(shape, 256, 256, (SELECT bound FROM boundtable)) AS pixel");
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW pixelaggregates AS\n" +
                "SELECT pixel, count(*) as weight\n" +
                "FROM pixels\n" +
                "GROUP BY pixel");
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW pixelaggregates AS\n" +
                "SELECT pixel as pixel, ST_Colorize(weight, (SELECT max(weight) FROM pixelaggregates)) as color\n" +
                "FROM pixelaggregates");
        sparkSession.sql("CREATE OR REPLACE TEMP VIEW images AS\n" +
                "SELECT ST_Render(pixel, color) AS image, (SELECT ST_AsText(bound) FROM boundtable) AS boundary\n" +
                "FROM pixelaggregates");

        ImageGenerator imageGenerator = new ImageGenerator();
        BufferedImage image = ((ImageSerializableWrapper) sparkSession.table("images").takeAsList(1).get(0).get(0)).getImage();
        imageGenerator.SaveRasterImageAsLocalFile(image, outputFile, ImageType.PNG);
    }
}